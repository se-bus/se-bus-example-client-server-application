import { on } from 'se-bus';
import { bindClient } from 'se-bus-ws';



export default async function() {
    const seBusClient = await bindClient(import.meta.env.VITE_WSS_URL);

    on('message.send', seBusClient.transfer('client.message.send'));
    seBusClient.listen('client.message.receive');
}