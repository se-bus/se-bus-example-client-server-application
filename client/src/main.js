import { createApp, onMounted } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'

import createSeBusBrige from '@/lib/se-bus.js';
createSeBusBrige();

import '~/bootstrap/dist/css/bootstrap.min.css'

const app = createApp(App)

app.use(createPinia())

app.mount('#app')
