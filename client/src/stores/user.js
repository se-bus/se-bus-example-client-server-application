import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user-store', () => {
  const username = ref(null);

  const setUsername = (to) => {
    if(to == username.value) return;
    username.value = to;
  }

  return { username, setUsername }
})
