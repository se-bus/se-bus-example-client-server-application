# Example se-bus application

## What is this?
This is a project demonstrating the usage of [se-bus](https://www.npmjs.com/package/se-bus) and [se-bus-ws](https://www.npmjs.com/package/se-bus-ws).  
It consists out of two applications; a server (subfolder "server") and a client (subfolder "client").  
The server is a simple, less then 50 lines, node.js application offering a se-bus bridge for the clients to communicate with in realtime.  
The client is a Vue.JS powered application connecting to that client and send chat messages over it.

## How to get it up?
clone this repo. Run in both, server and client, first `npm install`, then `npm run dev`.  
Afterwards there should be two ports opened: `5173` (default client) and `8000` (default server).  
Visit `http://localhost:5173/`, enter your desired username and send yourself messages. You can even open a new tab, enter the same address and chat with yourself.  
Messages ending with `(server)` in the sender field are those redirected by `se-bus-ws`

## Technical explanation
As this is a reference for the packages named above, here are a few more words about them.

### Client
In the client you'll see a few things quite handy while developing applications in vue/react/...

#### Local event handling (client)
Example: in `/client/src/components/ChatInput.vue` an event `message.send` will be emitted when you tipe a message.  
In `/client/src/components/Chat.vue` an eventStack will be generated listening to `message.send`.  
While that component is still active (eventStack is registered in `onMounted` and unregistered in `onUnmounted` chat messages will be stored).

#### Local event handling (server)
In `server/index.js` you'll find the three following lines:
```js
on('client.message.send', (data) => {
    emit('message.send', data);
});
```
As soon as the server receives an `client.message.send` event an `message.send` event will be emitted.

#### Bridging
Both, client and server, have a bridge connecting their `se-bus` event bus with each other

##### Client
On client-side the `se-bus-ws` brige is initialized in `/client/src/lib/se-bus.js`. Using `bindClient` a connection to the websocket-server of the server part is done, afterwards all local `message.send` events will be piped through it as `client.message.send` events.  
Also, incoming `client.message.receive` events emitted on the server side are allowed to pass the tunnel.

##### Server
The server also uses `se-bus-ws` brige is in `/server/index.js`. A new `se-brige-ws` server is initialized and configured to forward local `message.send` events from the server as `client.message.receive` events to the client.  
`client.message.send` events are also allowed to be emitted locally when a client passes them through the bridge.