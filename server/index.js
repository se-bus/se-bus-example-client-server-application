import { bindServer } from 'se-bus-ws';
import { emit, on } from 'se-bus';
import { createServer } from 'http';

(async () => {
    // Setup a http server
    const httpServer = createServer();
    // Bind our bridge on it
    const seBusServer = await bindServer(httpServer);

    /**
     * When receiving an event "message.send", broadcast it to clients as "client.message.receive"
     */
    on('message.send', seBusServer.transfer('client.message.receive')); 

    /**
     * Allow clients to send the event "client.message.send"
     * If you look in the client (sec/lib/se-bus.js) you'll see that in the client that event is broadcastet as "message.send"
     * The event name will be overriden by the bridge in the client
     */
    seBusServer.listen('client.message.send');

    /**
     * Emit local "client.message.send" events (coming from clients) as
     * "message.send" events.
     * Those will be passed to the client as "client.message.receive" events (see above)
     */
    on('client.message.send', (data) => {
        /**
         * Emit the event as message.send
         * See above - clients will receive an "client.message.receive"-event
         */
        emit('message.send', data);
    });

    // start http server
    httpServer.listen(8000);
})();